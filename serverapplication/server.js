var Auth0 = require('auth0');
var dotenv    = require('dotenv');
var express   = require('express');
var extend = require('xtend');

//Start Work with DB
var mongoose = require('mongoose');
mongoose.connect('mongodb://workspaceitdev1:Workspaceit123@ds035723.mongolab.com:35723/workspaceit_first_app');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('DB open');
});

var memberSchema = mongoose.Schema({
    name: String
});
var memberModel = mongoose.model('members', memberSchema);

var transectionSchema = mongoose.Schema({
  Auth0Token: String,
								stripeToken: String,
								amount: String,
								currency: String,
								customerId: String,
								sourceName: String,
								sourceID: String,
								chargeId: String,
								balanceTransactionId: String
});
var transectionModel = mongoose.model('transections', transectionSchema);
//End DB Work

var app       = express();

dotenv.load();

app.use(express.logger());

app.use('/', express.static(__dirname + '/'));

var api = new Auth0({
  domain:       process.env.AUTH0_DOMAIN || 'workspaceitdev1.auth0.com',
  clientID:     process.env.AUTH0_CLIENT_ID || 'gjRCeYKwMQ7wDTLElGVIQhTRpiyu0Dc6',
  clientSecret: process.env.AUTH0_CLIENT_SECRET || 'bkrnouXHM9rdsK0s4Gx_F2JzPSfZi6b2Sshet8yqPzXstfQ2Q22XUxWUOts2HdTL'
});

var CONNECTION = 'Username-Password-Authentication';

app.configure(function () {
  app.use(express.bodyParser());
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
});

app.post('/signup', function (req, res) {
  var data = extend(req.body, {connection: CONNECTION, email_verified: false});

  api.createUser(data, function (err) {
    if (err) {
      console.log('Error creating user: ' + err);
      res.send(500, err);
      return;
    }

	//save to DB
	var member = new memberModel({ name: 'fluffy' });
	member.save(function (err, fluffy) {
	  if (err) return console.error(err);
	  
	});
	
    res.send(200, {status: 'ok'});
  });
});

app.post('/getpayment', function (req, res) {
  api.getAccessToken(function (err, token) {
	  if (err) {
		console.log('Error fetching token: ' + err);
		return;
	  }
  
	function saveStripeCustomerId(authToken, stripeToken, charge){
		//charge = JSON.stringify(charge);
		console.log(charge.source.id+' --and --'+charge.source.name); 
		//save to DB
		var transection = new transectionModel({ 
								Auth0Token: token,
								stripeToken: stripeToken,
								amount: charge.amount,
								currency: charge.currency,
								customerId: charge.customer,
								sourceName: charge.source.name,
								sourceID: charge.source.id,
								chargeId: charge.id,
								balanceTransactionId: charge.balance_transaction
								});
		transection.save(function (err, tran) {
		  if (err) return console.error(err);
		  
		});
	}
	// Set your secret key: remember to change this to your live secret key in production
	// See your keys here https://dashboard.stripe.com/account/apikeys
	var stripe = require("stripe")("sk_test_kw86KDltDOydTZ0tolW9WK5C");

	// (Assuming you're using express - expressjs.com)
	// Get the credit card details submitted by the form
	var stripeToken = req.body.stripeToken;

	/*var charge = stripe.charges.create({
	  amount: 1000, // amount in cents, again
	  currency: "usd",
	  source: stripeToken,
	  description: "Example charge"
	}, function(err, charge) {
	  if (err && err.type === 'StripeCardError') {
		// The card has been declined
	  }
	});
	*/
	var customerId;
	stripe.customers.create({
	  source: stripeToken,
	  description: 'payinguser@example.com'
	}).then(function(customer) {
		customerId = customer.id;
	  return stripe.charges.create({
		amount: 2000, // amount in cents, again
		currency: "usd",
		customer: customer.id
	  });
	}).then(function(charge) {
		
		saveStripeCustomerId(token, stripeToken, charge);
	  //saveStripeCustomerId(user, charge.customer);
	});

    res.redirect(200,"https://workspacenodeclient.herokuapp.com/translist?msg=OK");
  });
});

//app.listen(3001);
//console.log('listening on port http://localhost:3001');

app.set('port', (process.env.PORT || 5000));
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});